import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { MeteoService } from '../meteo/services/meteo.service'
import { MeteoResult } from '../meteo/vo/meteo-result.vo'

@Component({
  selector: 'app-meteo-view',
  templateUrl: './meteo-view.component.html',
  styleUrls: ['./meteo-view.component.css']
})
export class MeteoViewComponent implements OnInit {
  public meteoResult: any;

  public ville: string = '';

  private isLoading : boolean = false

  constructor (private router: Router, private activateRoute: ActivatedRoute, private meteoService: MeteoService) {}

  ngOnInit (): void {
    this.activateRoute.queryParams.subscribe((params) => {
      console.info('params ', params)
      if (params && params.ville) {
        this.ville = params.ville
        this.updateVille(params.ville)
      }
    })
  }

  public searchChangeHandler (value: string): void {
    this.updateVille(value)
  }

  private updateVille (value:string) {
    if (value?.length >= 3) {
      this.isLoading = true
      this.meteoService.searchMeteo(value).then((result: MeteoResult) => {
        this.meteoResult = result
      }).catch(error => {
        console.error('Error ', error)
      }).finally(() => {
        this.isLoading = false
      })
    } else {
      this.meteoResult = undefined
    }
  }
}
