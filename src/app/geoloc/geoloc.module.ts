import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { MeteoGeolocComponent } from '../meteo-geoloc/meteo-geoloc.component'
import { RouterModule, Routes } from '@angular/router'
import { GeolocService } from './services/geoloc.service'
import { GeolocToCityService } from './services/geoloc-to-city.service'
import { HttpClientModule } from '@angular/common/http'

export const geolocRouteList: Routes = [
  {
    path: '**',
    component: MeteoGeolocComponent
  }
]

@NgModule({
  declarations: [MeteoGeolocComponent],
  providers: [GeolocService, GeolocToCityService],
  imports: [
    CommonModule,
    RouterModule.forChild(geolocRouteList),
    HttpClientModule
  ]
})
export class GeolocModule { }
