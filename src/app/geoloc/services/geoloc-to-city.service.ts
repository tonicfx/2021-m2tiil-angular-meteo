import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class GeolocToCityService {
  private API_KEY = 'd4fa32b1c4cc44b8a6bf0abb1d690313';

  private API_URL = 'https://api.opencagedata.com/geocode/v1/json?q={lat}+{lon}&key={apikey}';

  constructor (private readonly httpClient: HttpClient) {}

  public async searchCity (lat: number, lon: number): Promise<any> {
    const urlAPI = this.API_URL.replace('{lat}', lat.toString())
      .replace('{lon}', lon.toString())
      .replace('{apikey}', this.API_KEY)
    console.info('urlAPI ', urlAPI)
    const response: any = await this.httpClient.get(urlAPI).toPromise()
    if (response && response.results && response.results.length > 0) {
      let city = response.results[0].components.city
      if (city === undefined || city === null) {
        city = response.results[0].components.town
      }

      return city
    } else {
      return null
    }
  }
}
