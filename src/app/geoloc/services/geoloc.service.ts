import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class GeolocService {
  constructor () {}

  public getLocation (): Promise<any> {
    if (navigator.geolocation) {
      return new Promise((res, rej) => {
        window.navigator.geolocation.getCurrentPosition(res, rej)
      })
    } else {
      console.info('KO, Geoloc pas actif')
      return Promise.reject('KO, Geoloc pas actif')
    }
  }
}
