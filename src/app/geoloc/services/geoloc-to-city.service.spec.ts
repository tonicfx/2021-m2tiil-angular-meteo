import { TestBed } from '@angular/core/testing'

import { GeolocToCityService } from './geoloc-to-city.service'
import { HttpClientModule } from '@angular/common/http'

describe('GeolocToCityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [HttpClientModule],
      providers: []
    }).compileComponents()
  })

  it('should be created', () => {
    const service: GeolocToCityService = TestBed.get(GeolocToCityService)
    expect(service).toBeTruthy()
  })
})
