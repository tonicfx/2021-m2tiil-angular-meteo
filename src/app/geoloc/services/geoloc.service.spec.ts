import { TestBed } from '@angular/core/testing'

import { GeolocService } from './geoloc.service'
import { HttpClientModule } from '@angular/common/http'
import { doesNotMatch } from 'assert'

describe('GeolocService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [HttpClientModule],
      providers: []
    }).compileComponents()
  })

  it('should be created', () => {
    const service: GeolocService = TestBed.get(GeolocService)
    expect(service).toBeTruthy()
  })

  it('get coord properties', (done) => {
    const service: GeolocService = TestBed.get(GeolocService)
    service
      .getLocation()
      .then((result) => {
        expect(result).not.toBeNull()
        expect(result.coord).not.toBeNull()
        done()
      })
      .catch((error) => {
        fail()
      })
  })
})
