import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

const routes: Routes = [
  { path: '', redirectTo: 'geoloc', pathMatch: 'full' },
  {
    path: 'geoloc',
    loadChildren: () =>
      import('../geoloc/geoloc.module').then((m) => m.GeolocModule)
  },
  {
    path: 'meteo',
    loadChildren: () =>
      import('../meteo/meteo.module').then((m) => m.MeteoModule)
  }
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
