import { Component, OnInit, Input, SimpleChanges } from '@angular/core'
import { MeteoItem } from '../meteo/vo/meteo-item.vo'

@Component({
  selector: 'meteo-item-renderer',
  templateUrl: './meteo-itemrenderer.component.html',
  styleUrls: ['./meteo-itemrenderer.component.css']
})
export class MeteoItemrendererComponent implements OnInit {
  @Input()
  meteoItem: MeteoItem;

  @Input()
  indexOfElement: number;

  public effectDelay : number

  constructor () {}

  ngOnInit () {}

  ngOnChanges (changes: SimpleChanges) {
    console.info('indexOfElement ', this.indexOfElement)
    this.effectDelay = this.indexOfElement / 10
  }
}
