import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeteoMapComponent } from './meteo-map.component';

describe('MeteoMapComponent', () => {
  let component: MeteoMapComponent;
  let fixture: ComponentFixture<MeteoMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeteoMapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeteoMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
