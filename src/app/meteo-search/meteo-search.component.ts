import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'

@Component({
  selector: 'app-meteo-search',
  templateUrl: './meteo-search.component.html',
  styleUrls: ['./meteo-search.component.css']
})
export class MeteoSearchComponent implements OnInit {
  @Input('ville') ville: string = '';

  @Output('search')
  public searchChange: EventEmitter<string> = new EventEmitter<string>();

  ngOnInit (): void {
  }

  clickHandler (event:any) {
    this.searchChange.emit(this.ville)
  }

  keyDownHandler (event:any) {
    this.searchChange.emit(this.ville)
  }
}
