export interface MeteoCity {
  name: string;
  coord: any;
  population: number;
}
