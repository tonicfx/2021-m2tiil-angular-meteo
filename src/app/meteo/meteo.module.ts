import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { MeteoViewComponent } from '../meteo-view/meteo-view.component'
import { MeteoSearchComponent } from '../meteo-search/meteo-search.component'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { MeteoService } from './services/meteo.service'
import { MeteoItemrendererComponent } from '../meteo-itemrenderer/meteo-itemrenderer.component'
import { TemperaturePipe } from './pipe/temperature.pipe'
import { RouterModule, Routes } from '@angular/router'

import { MeteoMapComponent } from '../meteo-map/meteo-map.component'
import { MapComponent } from '../esri-map/map.component'
import { ArcgisApiService } from '../esri-map/services/arcgis-api.service'
import { GeolocToCityService } from '../geoloc/services/geoloc-to-city.service'

export const meteoRouteList: Routes = [
  {
    path: 'result',
    component: MeteoViewComponent
  },
  {
    path: 'map',
    component: MeteoMapComponent
  }
]

@NgModule({
  declarations: [MeteoViewComponent,
    MeteoSearchComponent,
    MeteoItemrendererComponent,
    TemperaturePipe,
    MeteoMapComponent, MapComponent],
  providers: [MeteoService, ArcgisApiService, GeolocToCityService],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(meteoRouteList)
  ],
  exports: [MeteoViewComponent]
})
export class MeteoModule { }
