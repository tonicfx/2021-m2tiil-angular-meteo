import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'temperature'
})
export class TemperaturePipe implements PipeTransform {
  transform (value: any, ...args: any[]): any {
    if (!isNaN(value)) {
      const fahrenheit = Math.round((value * 9) / 5) + 32

      let format = value >= 0 ? '+' : '-'
      format += `${value} ° C (${fahrenheit} ° F)`
      return format
    }

    return null
  }
}
