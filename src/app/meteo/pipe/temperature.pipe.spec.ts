import { TemperaturePipe } from './temperature.pipe'

describe('TemperaturePipe', () => {
  it('create an instance', () => {
    const pipe = new TemperaturePipe()
    expect(pipe).toBeTruthy()
  })

  it('valid positive temperature pipe format', () => {
    const pipe = new TemperaturePipe()
    expect(pipe.transform(9.98)).toBe('+9.98 ° C (50 ° F)')
  })

  it('valid negative temperature pipe format', () => {
    const pipe = new TemperaturePipe()
    expect(pipe.transform(-3.5)).toBe('-3.5 ° C (26 ° F)')
  })
})
