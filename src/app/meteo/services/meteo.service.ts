/* eslint-disable no-useless-constructor */
import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { MeteoResult } from '../vo/meteo-result.vo'

@Injectable({
  providedIn: 'root'
})
export class MeteoService {
  private API_KEY = '888f70e84a4d7e44f3c0d4870c926e9d';
  private METEO_URL =
    'http://api.openweathermap.org/data/2.5/forecast?q={VILLE},FR&APPID={API_KEY}&units=metric';

  constructor (private readonly httpClient: HttpClient) {}

  public searchMeteo (value: string): Promise<any> {
    const url: string = this.METEO_URL.replace('{VILLE}', value).replace(
      '{API_KEY}',
      this.API_KEY
    )

    return this.httpClient
      .get(url)
      .toPromise()
      .then((result) => {
        const meteoResult: MeteoResult = result as MeteoResult
        return meteoResult
      })
      .catch((error) => {
        console.error('error ', error)
        return undefined
      })
  }
}
