import { Component, OnInit } from '@angular/core'
import { GeolocToCityService } from '../geoloc/services/geoloc-to-city.service'
import { GeolocService } from '../geoloc/services/geoloc.service'

@Component({
  selector: 'app-meteo-geoloc',
  templateUrl: './meteo-geoloc.component.html',
  styleUrls: ['./meteo-geoloc.component.css']
})
export class MeteoGeolocComponent implements OnInit {
  public geolocInfo: any;

  public error : boolean;

  constructor (
    private readonly geolocService: GeolocService,
    private readonly geolocToCity: GeolocToCityService
  ) {}

  ngOnInit () {
    this.getPosition()
  }

  private async getPosition () {
    this.error = false
    this.geolocService.getLocation().then(async position => {
      if (position && position.coords) {
        console.info('position ', position)
        const ville = await this.geolocToCity.searchCity(
          position.coords.latitude,
          position.coords.longitude
        )
        this.geolocInfo = { ville: ville }
      } else {
        this.error = true
        this.geolocInfo = {}
      }
    }).catch(error => {
      console.info('error ', error)
      this.error = true
      this.geolocInfo = {}
    })
  }
}
