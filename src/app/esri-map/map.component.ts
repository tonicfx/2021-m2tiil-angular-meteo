import { Component, OnInit, Input } from '@angular/core'
import { ArcgisApiService } from './services/arcgis-api.service'
import { GeolocToCityService } from '../geoloc/services/geoloc-to-city.service'
import { ActivatedRoute, Router } from '@angular/router'
import { Route } from '@angular/compiler/src/core'

@Component({
  selector: 'esri-map',
  template: `
    <div [id]="id" [style.width]="width" [style.height]="height"></div>
  `,
  styles: []
})
export class MapComponent implements OnInit {
  sceneView: any;

  @Input() id: string;
  @Input() center: [number, number];
  @Input() width: string;
  @Input() height: string;
  @Input() zoom: number;
  @Input() basemap: string;
  @Input() title: string;

  constructor (
    private arcgisService: ArcgisApiService,
    private geolocToCityService : GeolocToCityService,
    private router : Router
  ) {}

  ngOnInit () {
    this.arcgisService.loaded$.subscribe(loaded => {
      if (loaded) {
        this.arcgisService
          .constructMap({ basemap: this.basemap, elevation: true })
          .then(map => {
            this.arcgisService
              .constructSceneView({
                map: map,
                container: this.id,
                center: [-4.5, 48.5],
                zoom: 8
              })
              .then(sceneView => {
                this.sceneView = sceneView
                this.sceneView.on('click', (event:any) => {
                  this.mapClickHandler(event)
                })
              })
          })
      }
    })
  }

  mapClickHandler (event:any) {
    if (event.mapPoint) {
      this.geolocToCityService.searchCity(event.mapPoint.latitude, event.mapPoint.longitude).then(result => {
        if (result !== undefined) {
          this.router.navigate(['meteo/result/'], { queryParams: { ville: result } })
        }
      })
    }
  }
}
